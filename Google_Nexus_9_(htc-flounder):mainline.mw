== Minimal working dts ==
The following is a minimal working device tree that is just enough to boot into the initramfs debug-shell. The boot parameter <code>clk_ignore_unused</code> is a temporary workaround to fix an issue where the image on the screen slowly distorts to static as soon as the kernel boots. A similar issue is discussed [https://github.com/NVIDIA/tegra-nouveau-rootfs/issues/22 here]. Do not edit the <code>google,flounder64</code> string, otherwise the bootloader will reject the dtb and refuse to boot the kernel.

<pre>// SPDX-License-Identifier: GPL-2.0
/dts-v1/;

#include "tegra132.dtsi"

/ {

	model = "Google Nexus 9";
	compatible = "google,flounder64", "nvidia,tegra132";

	aliases {
		serial0 = &uarta;
	};

	chosen {
		stdout-path = "serial0:115200n8";
		bootargs = "clk_ignore_unused PMOS_NO_OUTPUT_REDIRECT";
	};

	memory {
		device_type = "memory";
		reg = <0x0 0x80000000 0x0 0x80000000>;
	};

	clocks {
		compatible = "simple-bus";
		#address-cells = <1>;
		#size-cells = <0>;

		clk32k_in: clock@0 {
			compatible = "fixed-clock";
			reg=<0>;
			#clock-cells = <0>;
			clock-frequency = <32768>;
		};
	};

	serial@70006000 {
		status = "okay";
	};

};</pre>
